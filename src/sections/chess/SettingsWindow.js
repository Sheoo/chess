import React, { useState } from "react";
// import Warning from "./Warning";

const Settings = ({
  windowToDisplay,
  // handleSetSelection,
  // setsOfPieces,
  // pieces,
  // handleColorSelection,
  // colors,
  resetChessboard,
  manageSettingsWindow,
  historyLength,
  button,
}) => {
  const [warning, setWarning] = useState(false);
  const handleFading = (testResult) => ({
    opacity: testResult ? "1" : "0",
    transform: testResult ? "scale(1)" : "scale(0)",
    transitionProperty: "transform, box-shadow, opacity",
    transition: "transform 0s, box-shadow 300ms, opacity 300ms",
    transitionDelay: testResult ? "0s, 300ms, 300ms" : "300ms, 0s, 0s"
  });
  return (
    <div
      className="window center"
      style={handleFading(windowToDisplay === "settings")}
    >
      {/* <Warning
        warning={warning}
        setWarning={setWarning}
        resetChessboard={resetChessboard}
        button={button}
      /> */}
      {/* <div>
        {setsOfPieces.length > 1 ? (
          <select onChange={handleSetSelection}>
            {setsOfPieces.map((set, i) => (
              <option value={i} key={i}>
                {set.name}
              </option>
            ))}
          </select>
        ) : (
          <p>Only {pieces.name} set is available.</p>
        )}
        {colors.length > 1 ? (
          <select onChange={handleColorSelection}>
            {colors.map((color, i) => (
              <option value={i} key={i}>
                {color.name}
              </option>
            ))}
          </select>
        ) : (
          <p>Only {pieces.name} set is available.</p>
        )}
      </div> */}
      <div style={handleFading(!warning)}>
        <div
          className="center"
          style={{
            ...button,
            opacity: historyLength ? "1" : "0.5",
            cursor: historyLength ? "pointer" : "auto",
          }}
          onClick={() => (historyLength ? setWarning(true) : null)}
        >
          Reset Game
        </div>
        <div className="center" style={button} onClick={manageSettingsWindow}>
          Close
        </div>
      </div>
      <div style={handleFading(warning)}>
        <p>
          Are you sure you want to reset this game ? All moves from this game
          will be lost.
        </p>
        <div
          style={button}
          onClick={() => {
            setWarning(false);
            resetChessboard();
          }}
        >
          Yes
        </div>
        <div style={button} onClick={() => setWarning(false)}>
          No
        </div>
      </div>
    </div>
  );
};

export default Settings;
