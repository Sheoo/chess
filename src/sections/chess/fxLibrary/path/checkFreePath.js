export default (direction, start, end, board) => {
  let path = false;
  switch (direction) {
    case "up":
      path = start.y;
      path--;
      while (path && path > end.y) {
        !board[path][end.x].owned ? path-- : (path = false);
      }
      break;
    case "down":
      path = start.y;
      path++;
      while (path && path < end.y) {
        !board[path][end.x].owned ? path++ : (path = false);
      }
      break;
    case "left":
      path = start.x;
      path--;
      while (path && path > end.x) {
        !board[end.y][path].owned ? path-- : (path = false);
      }
      break;
    case "right":
      path = start.x;
      path++;
      while (path && path < end.x) {
        !board[end.y][path].owned ? path++ : (path = false);
      }
      break;
    case "upleft":
      path = [start.y, start.x];
      path[0]--;
      path[1]--;
      while (path && path[0] > end.y && path[1] > end.x) {
        if (!board[path[0]][path[1]].owned) {
          path[0]--;
          path[1]--;
        } else {
          path = false;
        }
      }
      break;
    case "upright":
      path = [start.y, start.x];
      path[0]--;
      path[1]++;
      while (path && path[0] > end.y && path[1] < end.x) {
        if (!board[path[0]][path[1]].owned) {
          path[0]--;
          path[1]++;
        } else {
          path = false;
        }
      }
      break;
    case "downleft":
      path = [start.y, start.x];
      path[0]++;
      path[1]--;
      while (path && path[0] < end.y && path[1] > end.x) {
        if (!board[path[0]][path[1]].owned) {
          path[0]++;
          path[1]--;
        } else {
          path = false;
        }
      }
      break;
    case "downright":
      path = [start.y, start.x];
      path[0]++;
      path[1]++;
      while (path && path[0] < end.y && path[1] < end.x) {
        if (!board[path[0]][path[1]].owned) {
          path[0]++;
          path[1]++;
        } else {
          path = false;
        }
      }
      break;
    case "leftCastling":
      path =
        !board[end.y][start.x - 1].owned &&
        !board[end.y][start.x - 2].owned &&
        !board[end.y][start.x - 3].owned
          ? start.x
          : false;
      break;
    case "rightCastling":
      path =
        !board[end.y][start.x + 1].owned && !board[end.y][start.x + 2].owned
          ? start.x
          : false;
      break;
    default:
      return false;
  }
  return typeof path !== "boolean" ? true : false;
};
