import checkFreePath from "./checkFreePath";

const haveKingOrRookMovedAlready = (history, color, direction) =>
  history.find(
    move =>
      move.includes(`${color}: E${color === "white" ? "1" : "8"}`) ||
      move.includes(
        `${color}: ${direction === "left" ? "A" : "H"}${
          color === "white" ? "1" : "8"
        }`
      ) ||
      move === `${color}: O-O-O` ||
      move === `${color}: O-O`
  );

const checkKingPath = (start, end) =>
  end.x >= start.x - 1 &&
  end.x <= start.x + 1 &&
  end.y >= start.y - 1 &&
  end.y <= start.y + 1
    ? true
    : false;

const checkCastling = (start, end, board, history) => {
  if (start.y === end.y) {
    const direction = end.x < start.x ? "left" : "right";
    if (
      ((end.x === start.x - 2 &&
        checkFreePath(`${direction}Castling`, start, end, board)) ||
        (end.x === start.x + 2 &&
          checkFreePath(`${direction}Castling`, start, end, board))) &&
      !haveKingOrRookMovedAlready(history, start.owned, direction)
    ) {
      return true;
    }
  }
  return false;
};

export { checkKingPath, checkCastling };
