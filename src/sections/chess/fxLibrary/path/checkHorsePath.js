export default (start, end) =>
  (end.x === start.x + 1 && end.y === start.y + 2) ||
  (end.x === start.x - 1 && end.y === start.y - 2) ||
  (end.x === start.x + 1 && end.y === start.y - 2) ||
  (end.x === start.x - 1 && end.y === start.y + 2) ||
  (end.x === start.x + 2 && end.y === start.y + 1) ||
  (end.x === start.x - 2 && end.y === start.y - 1) ||
  (end.x === start.x + 2 && end.y === start.y - 1) ||
  (end.x === start.x - 2 && end.y === start.y + 1)
    ? true
    : false;
