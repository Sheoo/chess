import makeMove from "./makeMove";
import {
  checkPawnPath,
  checkPawnTake,
  checkEnPassant
} from "./path/checkPawnPath";
import checkHorsePath from "./path/checkHorsePath";
import getPathType from "./path/getPathType";
import checkFreePath from "./path/checkFreePath";
import { checkKingPath, checkCastling } from "./path/checkKingPath";

export default (dangerousStart, dangerousEnd, board, history) => {
  let action = false;
  const start = { ...board[dangerousStart.y][dangerousStart.x] };
  const end = { ...board[dangerousEnd.y][dangerousEnd.x] };
  const direction = getPathType(start, end);

  switch (start.status) {
    case "P":
      if (checkPawnPath(board, start, end)) {
        action = "";
      } else if (checkPawnTake(start, end)) {
        action = "take";
      } else if (checkEnPassant(board, start, end, history)) {
        action = start.x === end.x + 1 ? "take enpassant-" : "take enpassant+";
      }
      if (typeof action === "string" && (end.y === 0 || end.y === 7)) {
        action += " upgrade";
      }
      break;

    case "H":
      if (checkHorsePath(start, end)) {
        action = end.owned ? "take" : "";
      }
      break;

    case "R":
      if (
        (direction === "up" ||
          direction === "down" ||
          direction === "left" ||
          direction === "right") &&
        checkFreePath(direction, start, end, board)
      ) {
        action = end.owned ? "take" : "";
      }
      break;

    case "B":
      if (
        (direction === "upleft" ||
          direction === "upright" ||
          direction === "downleft" ||
          direction === "downright") &&
        checkFreePath(direction, start, end, board)
      ) {
        action = end.owned ? "take" : "";
      }
      break;

    case "Q":
      if (direction && checkFreePath(direction, start, end, board)) {
        action = end.owned ? "take" : "";
      }
      break;

    case "K":
      if (checkKingPath(start, end)) {
        action = end.owned ? "take" : "";
      } else if (checkCastling(start, end, board, history)) {
        action = start.x > end.x ? "castling qs" : "castling ks";
      }
      break;

    default:
      break;
  }

  if (typeof action === "string") {
    return makeMove(board, start, end, action, history);
  }
  return false;
};
