import { convertHistoryToString } from "./convertHistory";
import deepCopy from "./deepCopy";

export default (dangerousBoard, start, end, action, dangerousHistory) => {
  const board = deepCopy(dangerousBoard, "board");
  const history = deepCopy(dangerousHistory, "history");

  board[start.y][start.x].status = "";
  board[start.y][start.x].owned = false;

  board[end.y][end.x].status = start.status;
  board[end.y][end.x].owned = start.owned;

  switch (true) {
    case action === "take enpassant-":
      board[start.y][start.x - 1].status = "";
      board[start.y][start.x - 1].owned = false;
      break;
    case action === "take enpassant+":
      board[start.y][start.x + 1].status = "";
      board[start.y][start.x + 1].owned = false;
      break;
    case action === "castling qs":
      board[end.y][3].status = board[start.y][0].status;
      board[end.y][3].owned = board[start.y][0].owned;
      board[start.y][0].status = "";
      board[start.y][0].owned = false;
      break;
    case action === "castling ks":
      board[end.y][5].status = board[start.y][7].status;
      board[end.y][5].owned = board[start.y][7].owned;
      board[start.y][7].status = "";
      board[start.y][7].owned = false;
      break;
    default:
      break;
  }

  history[history.length] = convertHistoryToString(
    start.x,
    start.y,
    end.x,
    end.y,
    start.owned,
    action
  );

  return {
    updatedBoard: board,
    updatedHistory: history,
    action,
    result: true
  };
};
