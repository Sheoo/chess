import React from "react";

const UpgradeWindow = ({ status, handleUpgrade, button }) => (
  <div
    className="window center"
    style={{ display: status === "upgrade" ? "block" : "none" }}
  >
    <p>Turn this pawn into:</p>
    <div className="allChoices">
      <div className="halfChoices">
        <div style={button} onClick={() => handleUpgrade("R")}>
          ROOK
        </div>
        <div style={button} onClick={() => handleUpgrade("H")}>
          KNIGHT
        </div>
      </div>
      <div className="halfChoices">
        <div style={button} onClick={() => handleUpgrade("B")}>
          BISHOP
        </div>
        <div style={button} onClick={() => handleUpgrade("Q")}>
          QUEEN
        </div>
      </div>
    </div>
  </div>
);

export default UpgradeWindow;
