import FullWindow from "./FullWindow";

export default {
  component: FullWindow,
  name: "Chess",
  path: "/chessboard"
}
