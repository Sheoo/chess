import React from "react";

const Warning = ({ warning, setWarning, resetChessboard, button }) => (
  <div
    className="window"
    style={{ display: warning ? "block" : "none" }}
  >
    <p>
      Are you sure you want to reset this game ? All moves from this game will
      be lost.
    </p>
    <div
      style={button}
      onClick={() => {
        setWarning(false);
        resetChessboard();
      }}
    >
      Yes
    </div>
    <div style={button} onClick={() => setWarning(false)}>
      No
    </div>
  </div>
);

export default Warning;
