import wKsvg from "./wK.svg";
import wQsvg from "./wQ.svg";
import wBsvg from "./wB.svg";
import wHsvg from "./wH.svg";
import wRsvg from "./wR.svg";
import wPsvg from "./wP.svg";
import bKsvg from "./bK.svg";
import bQsvg from "./bQ.svg";
import bBsvg from "./bB.svg";
import bHsvg from "./bH.svg";
import bRsvg from "./bR.svg";
import bPsvg from "./bP.svg";

export default {
  name: "Chrome Icons",
  wK: wKsvg,
  wQ: wQsvg,
  wB: wBsvg,
  wH: wHsvg,
  wR: wRsvg,
  wP: wPsvg,
  bK: bKsvg,
  bQ: bQsvg,
  bB: bBsvg,
  bH: bHsvg,
  bR: bRsvg,
  bP: bPsvg
};
