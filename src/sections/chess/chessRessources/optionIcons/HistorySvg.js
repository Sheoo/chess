import React from "react";

const HistorySvg = (props) => (
  <svg viewBox="0 0 48.001 40.382" {...props}>
    <g
      fill="none"
      stroke={props.color}
      strokeWidth={4}
      strokeLinejoin="round"
      strokeMiterlimit={10}
    >
      <path d="M24 36.002v-30" />
      <path
        strokeLinecap="round"
        d="M2 34.002V2.482c0-.539.947-.48 1.499-.48h13c3.037 0 6.305 1.716 7.75 4.118M46 34.002V2.482c0-.539.052-.48-.501-.48h-13c-3.043 0-6.807 1.703-8.25 4.116M3 35.002h13.936c2.93 0 6.017 1.338 7.564 3.38M46 35.002H32.064c-2.93 0-6.017 1.338-7.564 3.38"
      />
    </g>
  </svg>
);

export default HistorySvg;
