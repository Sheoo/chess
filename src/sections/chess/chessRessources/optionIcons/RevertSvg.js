import React from "react";

const RevertSvg = (props) => (
  <svg viewBox="0 0 49 38" {...props}>
    <g
      fill="none"
      stroke={props.color}
      strokeWidth={4}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeMiterlimit={10}
    >
      <path d="M2.25 19.143L18.821 2M47 19H2M2.25 19.143L18.79 36" />
    </g>
  </svg>
);

export default RevertSvg;
