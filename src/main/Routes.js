import React, { useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./Home";
import Header from "./Header";
import sections from "../sections";
import colors from "./colors";

const Routes = () => {
  const [mainColor, setMainColor] = useState(colors[3].main);
  const [secColor, setSecColor] = useState(colors[3].sec);

  const buttonStyle = {
    width: "130px",
    margin: "15px auto",
    padding: "10px 0px",
    textAlign: "center",
    cursor: "pointer",
    borderRadius: "30px",
    transition: "color 500ms, background-color 500ms",
    backgroundColor: secColor,
    color: mainColor
  };

  const [button, setButton] = useState(buttonStyle);

  const switchColor = (i) => {
    setMainColor(colors[i].main);
    setSecColor(colors[i].sec);
    setButton({
      ...button,
      backgroundColor: colors[i].sec,
      color: colors[i].main
    });
  };

  return (
    <BrowserRouter>
      <div
        style={{
          backgroundColor: mainColor,
          color: secColor,
          minHeight: "100vh"
        }}
      >
        <Header
          switchColor={switchColor}
          mainColor={mainColor}
          secColor={secColor}
          colors={colors}
          button={button}
        />
        <Switch>
          <Route exact path="/" component={Home} />
          {sections.map((section, i) => (
            <Route
              path={section.path}
              render={() => (
                <section.component
                  mainColor={mainColor}
                  secColor={secColor}
                  button={button}
                />
              )}
              key={i}
            />
          ))}
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default Routes;
