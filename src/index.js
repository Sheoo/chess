import React from "react";
import ReactDOM from "react-dom";
import Routes from "./main/Routes";
import * as serviceWorker from "./serviceWorker";
import "./scss/index.scss";

ReactDOM.render(<Routes />, document.getElementById("root"));

serviceWorker.unregister();
